from Point import Point

def getPoints():
	x = int(input("Enter x coordinate:"))
	y = int(input("Enter y coordinate:"))

	return x, y

#First Point
p1 = Point()
print('Enter first point:')
x, y = getPoints()
p1.set(x, y)

#Second Point
p2 = Point()
print('Enter second point:')
x, y = getPoints()
p2.set(x, y)

#Point to check for dimension
p3 = Point()
print("Enter third point:")
x, y = getPoints()
p3.set(x, y)

try:
	check = p1.checkTurn(p2, p3)

	if check == 1 :
		print("Left turn at p1 p2 p3")
	elif check == -1 :
		print("Right turn at p1 p2 p3")
	else :
		print("Collinear")
except Exception as e:
	print('Caught this error: ' + repr(e))
