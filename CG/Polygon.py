import math
from LinkedList import *
from Point import Point
from Line import Line

class Polygon:
	def __init__(self, length):
		self.length = length
		self.points = LinkedList()

	# def sortPoints(self, ll):
	# 	ll = sorted(ll, key= lambda pt: pt.x) # sort by x axis
	# 	self.points.append(ll[0]) #insert first point
	# 	test = ll.copy() # new list to manipulate
	# 	testPoint = ll[0] # testing point
	# 	test = test[1:]

	# 	for p in range(len(test)):
	# 		minSlope = None
	# 		tempPoint = 0

	# 		for index in range(len(test)):
	# 			denominator = testPoint.x - test[index].x
	# 			numerator = testPoint.y - test[index].y
	# 			if (denominator != 0):
	# 				slope = float(numerator)/float(denominator)
	# 			else:
	# 				slope = 0.0
	# 			# print(testPoint.get(),test[index].get(), slope, minSlope, index)
	# 			if minSlope == None or slope < minSlope:
	# 				minSlope = slope
	# 				tempPoint = index

	# 		# print(tempPoint, test[tempPoint].get())
	# 		testPoint = test[tempPoint]
	# 		self.points.append(testPoint)
	# 		del test[tempPoint]

	# def sortPoints(self, ll):
	# 	maxY = max(ll, key = lambda pt: pt.y)
	# 	right = []
	# 	left = []

	# 	for point in ll:
	# 		if (point.x < maxY.x):
	# 			right.append(point)
	# 		else:
	# 			left.append(point)
	# 	right = sorted(right, key= lambda pt: pt.x)
	# 	for p in right[::-1]:
	# 		self.points.append(p)

	# 	left = sorted(left, key= lambda pt: pt.x)

	# 	for p in left[::-1]:
	# 		self.points.append(p)

	def getMax(self):
		first = self.points.head.next
		maxY = self.points.head.data.x
		maxY = self.points.head.data.y
		while (first.next is not self.points.head):
			if first.data.x > maxY:
				maxY = first.data.x
			if first.data.y > maxY:
				maxY = first.data.y
			first = first.next

		if first.data.x > maxY:
			maxY = first.data.x
		if first.data.y > maxY:
			maxY = first.data.y

		return maxY, maxY

	def sortPoints(self, points) :
		# compute centroid
		cent=(sum([p.x for p in points])/len(points),sum([p.y for p in points])/len(points))
		# sort by angle
		points.sort(key=lambda p: math.degrees((- math.atan2(p.y-cent[1], p.x-cent[0])) % 2 * math.pi))
		return points

	def getPoints(self, points):
		ll = []
		for index in range(self.length):
			p = Point()
			p.set(int(points[index]['x']), int(points[index]['y']))
			ll.append(p)

		ll = self.sortPoints(ll)

		for pt in ll:
			self.points.append(pt)

	def checkConvex(self):
		first = self.points.head
		while (first.next is not self.points.head):
			turn = first.data.checkTurn(first.next.data, first.next.next.data)
			if turn != 1:
				return first.data.get()
			first = first.next

		turn = first.data.checkTurn(first.next.data, first.next.next.data)
		if turn != 1:
			return first.data.get()
		return True

	def checkPointInclusion(self, p):
		first = self.points.head
		while (first.next is not self.points.head):
			turn = p.checkTurn(first.data, first.next.data)
			if turn != 1:
				return first.data.get()
			first = first.next

		turn = p.checkTurn(first.data, first.next.data)
		if turn != 1:
			return first.data.get()
		return True

	def inclusionRayCastingMethod(self, p):
		print("\nFor points: ")
		self.points.printList()
		ray = self.createRay(p)
		first = self.points.head
		count = 0.00

		while (first.next is not self.points.head):
			l = Line()
			l.set(first.data, first.next.data)
			check = ray.checkIntersection(l)
			if check is 1:
				count += 1
			first = first.next

		l = Line()	
		l.set(first.data, first.next.data)
		check = ray.checkIntersection(l)

		if check is 1:
			count += 1

		if (count%2) != 0.0:
			return 1
		else:
			return 0

	def createRay(self, p):
		maxX, maxY = self.getMax()
		rayP2 = Point()
		randomNum = 3
		rayP2.set(maxX + randomNum, maxY)

		ray = Line()
		ray.set(p, rayP2)
		return ray

	def extremePoints(self, vertices, vertices_list):
		length = int(vertices)
		extreme_points = [1]* length

		for i in range(length):
			for j in range(length-1):
				if (i != j):
					for k in range(length-2):
						if (i != j and i != k and j != k):
							for l in range(length-3):
								if (i != j and i != k and i != l and j != k and j != l and k !=	l):
									p = Point()
									p.set(int(vertices_list[i]['x']), int(vertices_list[i]['y']))
									poly = Polygon(3) # create polygon class
									poly.getPoints([vertices_list[j], vertices_list[k], vertices_list[l]])

									inclusion = poly.checkPointInclusion(p)
									if (inclusion is True):
										# print(f"Inclusion of point {p.get()}")
										extreme_points[i] = 0

		temp = []
		print("For points:")
		for index in range(length):
			print((vertices_list[index]['x'], vertices_list[index]['y']))
			if (extreme_points[index] == 1):
				temp.append(vertices_list[index])

		self.length = len(temp)
		self.getPoints(temp)

	def extremeEdges(self, vertices, vertices_list):
		length = int(vertices)

		extreme_edges = []
		for i in range(length):
			pi = Point()
			pi.set(int(vertices_list[i]['x']), int(vertices_list[i]['y']))
			for j in range(length):
				flag = 1
				if (i != j):
					pj = Point()
					pj.set(int(vertices_list[j]['x']), int(vertices_list[j]['y']))
					for k in range(length):
						if (i != j and i != k and j != k):
							p = Point()
							p.set(int(vertices_list[k]['x']), int(vertices_list[k]['y']))
							
							turn = pi.checkTurn(pj, p)
							if not (turn == 0 or turn == 1):
								flag = 0
					if (flag == 1):
						print(f"Extreme Edges: {(i, j)}")
						extreme_edges.append((i, j))
		temp = []
		for i in range(len(extreme_edges)):
			if (extreme_edges[i][0] not in temp):
				temp.append(extreme_edges[i][0])
			if (extreme_edges[i][1] not in temp):
				temp.append(extreme_edges[i][1])

		extreme_points = []

		for i in range(len(temp)):
			extreme_points.append(vertices_list[temp[i]])

		self.length = len(extreme_points)
		self.getPoints(extreme_points)

	def giftWrap2D(self, vertices, vertices_list):
		length = int(vertices)
		min_y = 0
		print("For points:")
		for p in range(0, len(vertices_list)):
			vertices_list[p]['x'] = int(vertices_list[p]['x'])
			vertices_list[p]['y'] = int(vertices_list[p]['y'])
			print((vertices_list[p]['x'], vertices_list[p]['y']))
			if (vertices_list[p]['y'] < vertices_list[min_y]['y']):
				min_y = p

		min_point = vertices_list[min_y]
		sorted_vertices = [min_point]
		temp = vertices_list.copy()
		# temp.pop(min_y)

		while temp:
			min_angle = None
			min_index = None
			# print("start", min_point)
			for index in range(len(temp)):
				if (temp[index] != min_point):
					angle = math.atan2(temp[index]['y']-min_point['y'], temp[index]['x']-min_point['x'])
					angle = math.degrees(angle % 2 * math.pi)
					# print(temp[index], angle)
					if (min_angle == None or min_angle > angle):
						min_angle = angle
						min_index = index
			
			# print(temp[min_index], min_angle)
			if (temp[min_index] != sorted_vertices[0]):
				min_point = temp.pop(min_index)
				sorted_vertices.append(min_point)
			else:
				# print(temp[min_index])
				break

		self.length = len(sorted_vertices)
		self.getPoints(sorted_vertices)

		# print("Convex Hull:")
		# self.points.printList()

	# def dot(vA, vB):
 #        return vA[0]*vB[0]+vA[1]*vB[1]

 #    def ang(A,B,C):
 #        # Get nicer vector form
 #        vA = [(B[0]-A[0]), (B[1]-A[1])]
 #        vB = [(C[0]-B[0]), (C[1]-B[1])]
 #        # Get dot prod
 #        dot_prod = dot(vA, vB)
 #        # Get magnitudes
 #        magA = dot(vA, vA)**0.5
 #        magB = dot(vB, vB)**0.5
 #        # Get cosine value
 #        cos_ = round(dot_prod/(magA*magB),4)
 #        #print (cos_)
 #        # Get angle in radians and then convert to degrees
        
 #        angle = math.acos(cos_)
 #        # Basically doing angle <- angle mod 360
 #        ang_deg = math.degrees(angle)%360
    
 #        return ang_degdef dot(vA, vB):
 #        return vA[0]*vB[0]+vA[1]*vB[1]

	def grahamScan(self, vertices, vertices_list):
		length = int(vertices)
		min_y = 0
		print("for Points:")
		for p in range(length):
			vertices_list[p]['x'] = int(vertices_list[p]['x'])
			vertices_list[p]['y'] = int(vertices_list[p]['y'])
			print((vertices_list[p]['x'], vertices_list[p]['y']))
			if (vertices_list[p]['y'] < vertices_list[min_y]['y']):
				min_y = p

		ll = []
		for index in range(length):
			p = Point()
			p.set(int(vertices_list[index]['x']), int(vertices_list[index]['y']))
			ll.append(p)

		# sort by polar angle
		ll.sort(key=lambda p: math.atan2(p.y-vertices_list[min_y]['y'], p.x-vertices_list[min_y]['x']))

		stack = ll[0:2]
		i = 2
		while i < length:
			if (stack[len(stack) - 2].checkTurn(stack[len(stack) - 1], ll[i]) == 1):
				stack.append(ll[i])
				i += 1
			else:
				stack.pop(len(stack) - 1)

		print("Points in stack:")
		for p in stack:
			print(p.get())

		print("\n")

# "5" : [
# 		{
# 			"x" : "10",
# 			"y" : "10"
# 		},
# 		{
# 			"x" : "50",
# 			"y" : "20"
# 		},
# 		{
# 			"x" : "18",
# 			"y" : "60"
# 		},
# 		{
# 			"x" : "15",
# 			"y" : "40"
# 		},
# 		{
# 			"x" : "20",
# 			"y" : "25"
# 		}
# 	],
# 	"3" : [
# 		{
# 			"x" : "2",
# 			"y" : "2"
# 		},
# 		{
# 			"x" : "10",
# 			"y" : "10"
# 		},
# 		{
# 			"x" : "2",
# 			"y" : "10"
# 		}
# 	]