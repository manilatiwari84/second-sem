from Point import Point
from Line import Line

def getPoints():
	x = int(input("Enter x coordinate:"))
	y = int(input("Enter y coordinate:"))

	return x, y

#First Point
p1 = Point()
print('Enter first point:')
x, y = getPoints()
p1.set(x, y)

#Second Point
p2 = Point()
print('Enter second point:')
x, y = getPoints()
p2.set(x, y)

#Creating a line
l1 = Line()
l1.set(p1, p2)

#Point to check for dimension
p3 = Point()
print("Enter third point:")
x, y = getPoints()
p3.set(x, y)

#Point to check for dimension
p4 = Point()
print("Enter fourth point:")
x, y = getPoints()
p4.set(x, y)

#Creating a line
l2 = Line()
l2.set(p3, p4)
try:
	check = l1.checkIntersection(l2)
	
	if (check):
		print("Intersection")
	else:
		print("No intersection")
except Exception as e:
	print('Caught this error: ' + repr(e))
